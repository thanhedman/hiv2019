"""Run a single concurrency simulation. 

Provides function ``get_param_set`` needed to build population and run sim. 
Builds population in ``buildPopulation`` from given parameters.
Provides function ``onesim``, which can run a single concurrency simulation.

This code is an iteration of Sim.py first created by Alan G. Isaac by adding
sub-populations: CSW, client, miner, ART, PrEP to buildPopulation and onesim. 

"""
import numbers
import os.path as path
import  glob, pprint, math, tempfile, time, shutil
import numpy as np
from numpy.random import RandomState
from collections import defaultdict
import logging
import multiprocessing as mp
#new classes
from c_Person import Person
from c_Scheduler import Scheduler
from c_Partnership import Partnership
from c_Infection import stagedHIVfactory


ehg_epsilons =  tuple( 1 - 1.02*(math.exp(3.9*(xi/10. - 1))-.02) for xi in range(11) )
nOutGroups = 12


def run_simulations(params):
    """Return None. Run all pending simulation for this model.
    Input should be a dict of model-specific parameters.
    """
    for simparams in get_param_set(params):
        onesim(simparams)
        
def run_simulations_mp(params, workerpool=None):
    """
    :todo:
    To be improved, map all the parameter sets at once
    """
    if workerpool is None:
        n_processors = mp.cpu_count()
        print '{0} cores available'.format(n_processors)
        workerpool = mp.Pool(n_processors)
    workerpool.map(onesim, get_param_set(params))
    #for worker in workerpool: worker.join()
    return workerpool
        
def onesim(params):
    """Return None; setup and run one replicate
    (e.g., all daily iterations for 1 out of 100 replicates, for 1 value of epsilon)
    """
    sim_name = '{0}: {1}'.format(params['sScenarioName'], params['sim_name'])
    print '\n\nBegin ' + sim_name + '\n' + pprint.pformat(params)
    #create a temp file to hold the results of one simulation
    outfolder = params['outfolder']
    tempfh = tempfile.NamedTemporaryFile(mode='w', suffix='.out', dir=outfolder, delete=False)
    assert params.get('fout', None) is None
    params['fout'] = tempfh   #used by `record_output`
    tempfname = tempfh.name   #we'll rename this if the simulation runs to completion
    #time the simulation
    t0 = time.clock()
    schedule = Scheduler(params=params)  #event scheduler (transmissions, deaths, dissolutions)
    nMales, nFemales = params['nMF']
    nSimDays = params['nSimDays']
    nBurnDays = params['nBurnDays']
    nOutputInterval = params['nOutputInterval']  # usu. 365 (i.e., once a year)
    # type check parameters
    assert type(nMales) is int
    assert type(nFemales) is int
    assert type(nSimDays) is int
    assert type(nBurnDays) is int
    assert type(nOutputInterval) is int

    #create the disease for this model
    # note: don't add factory to model_params as it won't pickle,
    #       which create MP problems
    params['Disease'] = stagedHIVfactory(durations=params['durationsHIV'],
                                    transM2F=params['beta_M2F'],
                                    transF2M=params['beta_F2M'])

    params['sim_phi'] = lambda male, female: params['model_phi'](male, female, params['epsilon'])
    params['counters'] = counter = defaultdict(int)

    #prepare outfile by writing header
    header = 'nMinfect,nFinfect,'
    header += 'MPrimaryTrans,FPrimaryTrans,MAsymptomaticTrans,FAsymptomaticTrans,MSymptomaticTrans,FSymptomaticTrans,'
    header += 'MPships,,,,,,,,,,,,FPships,,,,,,,,,,,,iMPships,,,,,,,,,,,,iFPships,,,,,,,,,,,,'
    header += 'MPrimary,FPrimary,'
    header += 'n_infected_CSWclients,n_infected_CSWs,'
    header += 'nMART,nFART,nMPrEP,nFPrEP,comPships,'
    header += 'CSWART,CSWPrEP,minerART,minerPrEP'
    tempfh.write(header)

    # Prepare variables (note that `males` and `females` not reused across simulations)
    males = list(Person(sex='M', registry=schedule, params=params) for i in range(nMales))
    females = list(Person(sex='F', registry=schedule, params=params) for i in range(nFemales))
    
    #add miners and CSWs
    nMiners = int(params['p_miners']*nMales)
    nCSW = int(params['p_nsexworkers']*nFemales)
    
    minerIds = np.random.choice(nMales, nMiners)
    for i in minerIds:
        males[i].miner = True
        
    #set clients of CSWs
    nMinerClients = int(nMiners * params['p_nclients_miner'])
    minerClientsIds = np.random.choice(minerIds, nMinerClients)
    normalMalesIds = np.setdiff1d(range(nMales), minerClientsIds)
     
    normalMalesClientsIds = np.random.choice(normalMalesIds, int((nMales-nMiners) * params['p_nclients']))
    allMaleClientsIds=np.union1d(minerClientsIds, normalMalesClientsIds)
    for i in allMaleClientsIds:
        males[i].CSWclient = True
    
    CSWIds = np.random.choice(nFemales, nCSW)
    for i in CSWIds:
        females[i].CSW = True

    #begin simulation loop /* Do the simulations */
    for day in range(nSimDays+nBurnDays):
        logging.info('\nBEGIN ITERATION for day {0}.'.format(day))
        logging.debug(schedule.show_one_day(day))
        
        #set annual growth of ART and PrEP
        if day % 365 == 0:
            params['p_nM_ART'] = params['p_nM_ART_0'] + params['p_nM_ART_YG']*(day//365)
            params['p_nF_ART'] = params['p_nF_ART_0'] + params['p_nF_ART_YG']*(day//365)
            params['p_PREP']   = params['p_PREP_0'] + params['p_PREP_YG']*(day//365)            
        

        # Seed infections after nBurnDays have passed (ONCE)
        if(day == nBurnDays):
            assert schedule.count_scheduled_deaths() == 0
            diseases = seedInfections(males, females, day, schedule=schedule, params=params)
            assert schedule.count_scheduled_deaths() == len(diseases)  #for now, disease is fatal
            assert all(deathday >= day for deathday in schedule.deaths)

        #run the core of the simulation (runs even during burn days)
        #params holds counter and fout
        schedule.coresim(
            males=males,
            females=females,
            day=day,
            params=params,
            CSWIds=CSWIds,
            allMaleClientsIds=allMaleClientsIds
            )

        # Record the output once a "period" (i.e., every nOutputInterval days)
        # :note: this won't record after last year is run (it needs one more day to pass the test).
        #        We keep it this way just to match EHG.
        if( day >= nBurnDays and (day-nBurnDays) % nOutputInterval == 0 ):
            print '.' ,
            outIndex = (day - nBurnDays) / nOutputInterval
            #    params holds counter and fout
            record_output(males, females, params)
            #reset counter
            counter.clear()  #reuse counter each day (since wrote contents to disk)

    #END of simulation; just need to clean up: reset static elements
    # prepare classes for reuse
    schedule.clear_partnerships() # clears the `partnerships` and `transmissions` multimaps
    schedule.deaths.clear()

    tempfh.close()
    dt = time.clock() - t0
    #since the simulation ran to completion, we can use the output file
    outfilename = params['outfilename']
    shutil.move(tempfname, outfilename)
    msg = """
    {0} completed successfully in {1:d} minutes.
    {0} output written to {2}.
    """.format(sim_name, int(dt)//60, outfilename)
    logging.info(msg)
    
def get_param_set(params):
    """Yield dict, a replicate specific set of parameters.
    There are nSim (e.g., 100) replicates for each epsilon,
    and each one has its own random seed, sim_name, and outfilename.
    This generator function should yield dicts with **pickleable** elements only,
    so that it can be used with multiprocessing.
    """
    outfolder = params['outfolder']
    outfile_pattern = 'eps??sim??.out'
    search_pattern = path.normpath(path.join(outfolder, outfile_pattern))
    previously_done = glob.glob(search_pattern)
    for n in range(params['nSim']):
        for i,eps in enumerate(ehg_epsilons):  #ehg_epsilons is a global constant
            assert type(eps) is float
            sim_name = 'eps{0:02d}sim{1:02d}'.format(i,n)
            outfilename = path.normpath(path.join(outfolder, sim_name + '.out'))
            if path.normpath(outfilename) in previously_done:
                print 'Skip simulation ' + sim_name + ' (already completed).'
                continue
            else:
                seed = params['nRndSeed'], n, int(eps * 10**5)   #replicate(n)-specific seed
                simparams = params.copy()
                simparams.update(
                    prng = RandomState(seed=seed),
                    epsilon = eps,  # `phi` needs this!
                    sim_name = sim_name,
                    outfilename = outfilename,
                    )
                yield simparams
                
def seedInfections(males, females, day, schedule, params):
    """Return diseases; seed initial infections.
    """
    assert not any(f.is_infected for f in females)
    assert not any(m.is_infected for m in males)
    logging.info('Seed infections.')
    Disease = params['Disease']
    nMales = len(males)
    nFemales = len(females)
    #note: using pHIVseed instead of pctHIVseed
    _pmSeedHIV, _pfSeedHIV = params['pSeedHIV']
    nMaleSeed = int(round(nMales * _pmSeedHIV))
    nFemaleSeed = int(round(nFemales * _pfSeedHIV))
    if not(nFemaleSeed < len(females) and nMaleSeed < len(males)):
        raise ValueError('choose smaller seeds')
    _prng = params['prng']
    idxF = _prng.permutation(nFemales)[:nFemaleSeed]
    idxM = _prng.permutation(nMales)[:nMaleSeed]
    diseases = list()
    for idx in idxF:
        diseases.append(females[idx].HIVSeedInfect(Disease, day, schedule))
    for idx in idxM:
        diseases.append(males[idx].HIVSeedInfect(Disease, day, schedule))
    assert nFemaleSeed == sum(f.is_infected for f in females)
    assert nMaleSeed == sum(m.is_infected for m in males)
    return diseases

def record_output(males, females, params):
    """Return None. Write data to disk.
    :note: EHG store these values in arrays; we write them to disk instead
    :note: we record the same values (in same order) as EHG,
      then we append some additions
    """
    counter = params['counters']  #count transmissions by stage
    fout = params['fout']  #the output file handle
    n_infected_males = sum(m.is_infected for m in males)
    n_infected_females = sum(f.is_infected for f in females)
    n_infected_CSWclients = sum((m.is_infected and m.CSWclient) for m in males)
    n_infected_CSWs = sum((f.is_infected and f.CSW) for f in females)
    #nMART, nFART, nMPrEP, nFPrEP
    nMART = sum((m.ART) for m in males)
    nFART = sum((f.ART) for f in females)
    nMPrEP = sum((m.PrEP) for m in males)
    nFPrEP = sum((f.PrEP) for f in females)
    
    #nCSWART,nCSWPrEP,nminerART,nminerPrEP
    nCSWART = sum((f.CSW and f.ART) for f in females)
    nCSWPrEP = sum((f.CSW and f.PrEP) for f in females)
    nminerART = sum((m.miner and m.ART) for m in males)
    nminerPrEP = sum((m.miner and m.PrEP) for m in males)
    
    #print("Infected CSWs: %d" % n_infected_CSWs)
    #print("Infected CSWclients: %d" % n_infected_CSWclients)
    #build up this period's data as a list
    data = [n_infected_males, n_infected_females]
    data += [
            counter['malePrimaryTransToday'],
            counter['femalePrimaryTransToday'],
            counter['maleAsymptomaticTransToday'],
            counter['femaleAsymptomaticTransToday'],
            counter['maleSymptomaticTransToday'],
            counter['femaleSymptomaticTransToday'],
            ]
    maleDistPartnerships = np.bincount(
        [male.n_partners for male in males],
        minlength=nOutGroups
        )
    assert sum(maleDistPartnerships) == len(males)
    femaleDistPartnerships = np.bincount(
        [female.n_partners for female in females],
        minlength=nOutGroups
        )
    assert sum(femaleDistPartnerships) == len(females)
    maleDistHIVp = np.bincount(
        [male.n_partners for male in males if male.is_infected],
        minlength=nOutGroups
        )
    femaleDistHIVp = np.bincount(
        [female.n_partners for female in females if female.is_infected],
        minlength=nOutGroups
        )
    #the following were provided as output arrays in the EHG code
    for dist in maleDistPartnerships, femaleDistPartnerships, maleDistHIVp, femaleDistHIVp:
        data.extend(dist[:nOutGroups])
        if (len(dist) > nOutGroups):
            logging.warn('discarding very high partnership counts')
    assert len(data) == 2 + 6 + 4*nOutGroups
    assert all(isinstance(item, numbers.Integral) for item in data), \
        "non-integer data!\n{}".format([(item,type(item)) for item in data]) #chkchkchkchk
    #above shd match EHG's output; below are additions
    nMprimary = sum(1 for male in males if male.has_primary())
    nFprimary = sum(1 for female in females if female.has_primary())
    data += [nMprimary, nFprimary]
    
    data += [n_infected_CSWclients, n_infected_CSWs]# add infected CSWs and clients
    data += [nMART, nFART, nMPrEP, nFPrEP]# add count of nMART, nFART, nMPrEP, nFPrEP
    data += [ counter['comPships'] ]# add count of formed commercial pships
    data += [nCSWART,nCSWPrEP,nminerART,nminerPrEP] # add counters for PrEP and ART
    #finally, write the data to file
    fout.write('\n')
    data = ','.join(str(d) for d in data)
    fout.write(data)
    
if __name__=="__main__":
    pass
    

