"""
Extend class isaac/sim.py
Added functions:
tally_transmission
random_pairings
Reason: use class Partnership


Contains Scheduler object that maintains a schedule
of future events and keeps a registry of other objects.

Modified treatment and tracking of agents. 

"""
import logging
from collections import defaultdict
import numpy as np

#clean import of the ./isaac/sim.py/Scheduler class ONLY
from isaac.sim import Scheduler as aiScheduler # change name to avoid problems
from c_Partnership import Partnership
from c_Person import Person


class Scheduler(aiScheduler):
    """Provides a scheduler object that
    maintains a schedule of future events
    and also keeps a registry of other objects.
    """
    def __init__(self, params):
        #Init parent class
        aiScheduler.__init__(self, params)
        
    #print object properties
    def __str__(self):
        return """
        I am a {cls} class.
        """.format(**dict(            
            cls=self.__class__           
            ))
    def count_partnerships(self):
        """Count only non commercial partnerships
        """
        
        prim_sec_pships = 0        
        for lst in self.dissolutions.values():
            for pship in lst[:]:
                if pship.tag !='comsex':
                    prim_sec_pships +=1
        #print prim_sec_pships
        return prim_sec_pships 
      
    def coresim(self, males, females, day, params, allMaleClientsIds=[], CSWIds=[]):
        """Return None; modify `params`.
        Run the core stages of the simulation (one iteration):
        form partnerships, 
        do HIV transmissions, 
        dissolve partnerships, 
        do HIV deaths,
        start treatment.
        These are run every iteration (including burn days).  
        """
        self.form_partnerships(males, females, day)
        self.form_COM_partnerships(males,females, day, allMaleClientsIds, CSWIds)
        self.hiv_transmissions(day)
        self.hiv_deaths(day)
        self.start_treatment(males, females, day, params)
        self.dissolve_partnerships(day)
        
    #################   MAIN SCHEDULE   ##################################
    def form_COM_partnerships(self, males, females, day, allMaleClientsIds, CSWIds):
        """form commercial partnerships, between CSW and CSWcustomers
        """
        logging.debug('coresim: BEGIN form commercial partnerships')
        params = self.params
        counter=params['counters']#dict for counting parameters
        
        nClientsArr = np.random.poisson(params['rho_csw'], len(CSWIds))
        nNewPartnerships = np.sum( nClientsArr )
        
        formedPartnerships = 0
        for i, n in zip(CSWIds, nClientsArr):
            #assign n clients to every CSW
            femaleCSW=females[i]
            for _ in range(n):
                #find random partner that is not in CSW's partner list already
                while True:
                    #find new partner 
                    clientID=int(np.random.choice(allMaleClientsIds, 1))
                    maleClient=males[clientID]
                    if femaleCSW not in maleClient.partners:
                        break
                    
                newpartnership = Partnership(maleClient, femaleCSW, day, registry=self, params=params, tag='comsex')  # form partnership
                formedPartnerships +=1
                
        assert nNewPartnerships == formedPartnerships, "form_COM_partnerships: number of formed and actual partnerships mismatch %d, %d" % (nNewPartnerships, formedPartnerships)
        logging.debug('coresim: END form commercial partnerships')
        logging.info('\t{0} partnerships formed'.format(formedPartnerships))
        #print "COM partnerships formed: ", formedPartnerships
        counter['comPships'] += formedPartnerships
        
    def form_partnerships(self, males, females, day): #chkchkchk schedule partnerships
        """
        copy of the old function
        must have to include new - Partnership - class!
        """
        params = self.params
        ##### form partnerships
        logging.debug('coresim: BEGIN form partnerships')
        old_partnership_count = self.n_partnerships   #chkchkchk
        # this chk is not required any more, total no of pships is larger for commercial pships...
        #assert self.count_partnerships() == old_partnership_count, "bad partnership count"
        # population sizes
        nMales = len(males); nFemales = len(females);
        #We follow EHG and impose a hard ceiling on the number of partnerships!
        # (There a lots of things not to like about this.)
        # There is a maximum of about one partnership per person. (vs MK2000)
        max_new_partnerships = (nMales+nFemales)//2 - self.count_partnerships()
        assert type(max_new_partnerships) is int, "max_new_partnerships is not int"
        # determine how many new partnerships to form (binomial draw with probability rho)
        prng = params['prng']
        rho = params['rho']     #aggregate partnership formation paramter
        assert type(rho) is float, "rho is not float"
        nFormPartnerships = 0  # needed to match EHG behavior (R's rbinom(0, rho)=0)
        if max_new_partnerships > 0:
            nFormPartnerships = prng.binomial(n=max_new_partnerships, p=rho)
        # form exactly nFormPartnerships new partnerships, but skip miners in mine
        npairings = 0  #counter for testing that `random_pairings` returns the right number - skip miners in mine => less pairs
        #:note: random pairings uses phi, which is in `params`
        for male, female in self.random_pairings(nFormPartnerships, males, females, params):
            if male.miner and (365-params['days_mining'])<(day % 365):
                pass
            else:
                newpartnership = Partnership(male, female, day, registry=self, params=params)  # form partnership
                npairings += 1
        #assert npairings == nFormPartnerships
        logging.debug('coresim: END form partnerships')
        logging.info('\t{0} partnerships formed'.format(npairings))
        
    def hiv_transmissions(self, day):
        """
        copy of the function to include:
        tally_transmission function and Person class
        properly (disentangled from isaac)
        """
        params = self.params
        ##### do HIV transmissions
        logging.debug('coresim: begin HIV transmission')
        #recall: schedule.transmissions maps days to lists of partnerships
        Disease = params['Disease']
        transmissions4today = self.transmissions[day]  #list of partnerships
        ntrans_today = len(transmissions4today)
        ntrans_waiting = self.count_transmissions()
        logging.info('do {0} of {1} transmissions'.format(ntrans_today, ntrans_waiting))
        for pship in transmissions4today:
            transmitter, disease = pship.transmit(Disease, day) #chk is there a better way?
            if transmitter is not None:
                assert disease is not None
                self.tally_transmission(day=day, transmitter=transmitter, counter=params['counters'])
        del self.transmissions[day]
        del transmissions4today
        # lose ntrans_today future transmissions but may have gotten some new via infections at transmission
        assert self.count_transmissions() >= ntrans_waiting - ntrans_today
        logging.debug('coresim: end HIV transmission')

    def tally_transmission(self, day, transmitter, counter):
        """Return None; tally the transmission.
        :note: EHG increment these in partnership.py; we do this instead
        
        copy of the isaac.sim.tally_transmission function
        added as method of the class Scheduler
        """
        assert isinstance(day, int)
        assert isinstance(transmitter, Person)
        assert isinstance(counter, defaultdict)
        # get the infection stage of the transmitting partner
        # and tally the stage for output
        stage = transmitter.get_HIV_stage(day);
        if transmitter.sex == 'M':
            if(stage == 'primary'):
                counter['malePrimaryTransToday'] += 1
            elif(stage == 'asymptomatic'):
                counter['maleAsymptomaticTransToday'] += 1
            elif(stage == 'sympotmatic'):
                counter['maleSymptomaticTransToday'] += 1
        elif transmitter.sex == 'F':
            if(stage == 'primary'):
                counter['femalePrimaryTransToday'] += 1
            elif(stage == 'asymptomatic'):
                counter['femaleAsymptomaticTransToday'] += 1
            elif(stage == 'sympotmatic'):
                counter['femaleSymptomaticTransToday'] += 1
        else:
            raise ValueError('Unknown sex: ' + str(transmitter.sex))
       
    def random_pairings(self, n_pairs, males, females, params):
        """Return n_pairs random male-female pairs (**with** replacement).
    
        n_pairs : int
            the number of pairs to form
        males : list
            the potential male partners
        females : list
            the potential female partners
        params : dict
            params['prng'] is the random number generator;
            params['phi'] is a function that returns partnership formation probability 
        
        copy of the isaac.sim.random_pairings function
        added as method of the class Scheduler
        """ 
        _phi = params['sim_phi']  #e.g., phiEGH in parameters.py
        _prng = params['prng']
        _randint = _prng.randint
        _random_sample = _prng.random_sample
        if n_pairs < 0:
            raise ValueError('cannot form a negative number of pairs')
        nMales = len(males)
        nFemales = len(females)
        assert n_pairs < nMales and n_pairs < nFemales
        while (n_pairs > 0):
            # `n_pairs` random males and females (*with* replacement)
            males2pair = (males[idxM] for idxM in _randint(0, nMales, n_pairs))
            females2pair = (females[idxF] for idxF in _randint(0, nFemales, n_pairs))
            draws = _random_sample(n_pairs)
            for (male, female, draw) in zip(males2pair, females2pair, draws):
                if(draw < _phi(male, female)): 
                    if female not in male.partners:
                        assert male not in female.partners
                        n_pairs -= 1
                        yield male, female
                 
    def start_treatment(self, males, females, day, params): 
        #print "Day: ", day
        """Pick selected persons every day and give them treatment accordingly
        note: a dying treated person is not automatically replaced by another treated one;"""
        p_nM_ART = params.get('p_nM_ART',0.0)
        p_nF_ART = params.get('p_nF_ART',0.0)
        p_PREP = params.get('p_PREP',0.0)
        nF_ART = sum(1 for female in females if female.ART) #female.is_infected and 
        nM_ART  = sum(1 for male in males if male.ART )  #male.is_infected and 
        nART   = nF_ART + nM_ART
        nFPREP = sum(1 for female in females if female.PrEP)    #female.is_infected and 
        nMPREP = sum(1 for male in males if male.PrEP)  #male.is_infected and 
        nPREP  = nFPREP + nMPREP
    
        #Remaining allowed ART
        newARTm = int(round(p_nM_ART * len(males) - nM_ART))
        newARTf = int(round(p_nF_ART * len(females) - nF_ART))
        #Treat some of them - randomly picked
        if newARTm > 0:
            #Get available nonART infected males
            nAm = [idx for idx in range(len(males)) if males[idx].is_infected and not males[idx].ART and not males[idx].PrEP]
            #print(day)
            #print(len(nAm))
            #print(sAm)
            #print('---')
            if len(nAm)>0:
                #Randomly decide how many ART to start today (per sex) among the remaining
                sAm = np.random.choice(min(len(nAm),newARTm)) + 1
                #Get the indices of the lucky males
                sAmi = np.random.choice(nAm, sAm, replace = False)
                #Actually start their treatment
                for idx in sAmi:
                    males[idx].ART = True
                #pass
        #Start treating some females
        if newARTf > 0:
            #Get available nonART infected females
            nAf = [idx for idx in range(len(females)) if females[idx].is_infected and not females[idx].ART and not females[idx].PrEP]
            if len(nAf)>0:
                #Randomly decide how many ART to start today (per sex) among the remaining
                sAf = np.random.choice(min(len(nAf),newARTf)) + 1
                #Get the indices of the lucky males
                sAfi = np.random.choice(nAf, sAf, replace = False)
                #Actually start their treatment
                for idx in sAfi:
                    females[idx].ART = True
                #pass
    
        #Remaining allowed PrEP
        newPrEP = int(round((p_PREP * (len(males) + len(females)) - nPREP)/2))  #remaining is halved between M/F
        #Treat some of them - randomly picked
        if newPrEP > 0:
            #Get available nonPREP non infected males
            nPm = [idx for idx in range(len(males)) if not males[idx].is_infected and not males[idx].ART and not males[idx].PrEP]
            #Randomly decide how many PREP to start today (per sex) among the remaining
            sPm = np.random.choice(newPrEP) + 1
            if len(nPm)>0:
                #Get the indices of the lucky males
                sPmi = np.random.choice(nPm, sPm, replace = False)
                #Actually start their treatment
                for idx in sPmi:
                    males[idx].PrEP = True
                #pass
                #Get available nonPREP non infected females
                nPf = [idx for idx in range(len(females)) if not females[idx].is_infected and not females[idx].ART and not females[idx].PrEP]
                #Randomly decide how many PREP to start today (per sex) among the remaining
                sPf = np.random.choice(newPrEP) + 1
                #Get the indices of the lucky males
                sPfi = np.random.choice(nPf, sPf, replace = False)
                #Actually start their treatment
                for idx in sPfi:
                    females[idx].PrEP = True
                #pass

    def hiv_deaths(self, day):
        logging.info('begin: HIV deaths')
        deaths4today = self.deaths[day]
        logging.info('do {0} deaths'.format(len(deaths4today)))
        assert len(deaths4today)==len(set(deaths4today))
        for indiv in tuple(deaths4today): #don't really need a copy (chk Person.die)
            #do not kill if taking ART
            if not indiv.ART:
                self.kill(indiv)
                #print "----kill:----"
            #print indiv, indiv.ART
        del self.deaths[day]
        del deaths4today
        logging.info('end: HIV deaths')


        
#---------------------------------------------------
#                 Test the class
#---------------------------------------------------  

if __name__=="__main__":
    pass
