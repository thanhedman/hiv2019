"""
This file extends class isaac.Partnership ONLY.
"""
import logging

#clean import of the ./isaac/concurrency.py/Person class only 
from isaac.concurrency import Partnership as aiPartnership

class Partnership(aiPartnership):
    """Provides a paired heterosexual "partnership" (i.e., sexual
    relationship). A `Parntership` has a predetermined random duration and it
    schedules HIV transmission events.

    Parntership is initialized with a `start_date`; its duration is determined
    during initialization, using a geometric distribution parameterized by
    `sigma` which must be provided in the params dict). The mean duration (in
    days) of the partnership is simply 1/sigma.

    :override: 
    Add optional tag parameter. If tag == 'comsex' partnership will last one
    day. 

    """
    def __init__(self, male, female, day, registry, params, tag=None):
        """Override parent init! 
        Return None. Initialize partnership,
        schedule partnership dissolution, 
        schedule disease transmission."""
        self.params=params
        #aiPartnership.__init__(self, male, female, day, registry, params)
        logging.debug('ENTER: Partnership.__init__')
        assert (male.sex=='M' and female.sex=='F')
        self._mf = male, female 
        self._prng = params['prng']
        self.start_date = day
        if tag == 'comsex':
            self.set_tag(params, tag='comsex')
            self.end_date= day + 1 #one day long 
            self.transmission_scheduled = False #T/F HIV transmission scheduled? 
            self.transmission_date = None   #date of scheduled transmission
        else:
            self.set_tag(params) #must come before duration  
            self.end_date = day + self.pshipduration(params)
            self.transmission_scheduled = False 
            self.transmission_date = None
        self.registry = registry    #register pship if possible 
        if registry is not None: 
            registry.register_partnership(self)
        male.add_partnership(self)
        female.add_partnership(self)
        self.expose_transmission(day)
        logging.debug('EXIT: Partnership.__init__')

    def set_tag(self, params, tag=None):
        """Allow only one primary partner (check union of pship sets)"""
        if tag==None:
            male, female = self._mf 
            has_primary = any(p.tag=='primary' for p in (male.partnerships + female.partnerships))
            if has_primary:
                self.tag = 'secondary'
                #set the relative frequency for non 'primary' partners
                self.sexfreq = params.get('secondarySexfreq', 1)
            else: 
                self.tag = 'primary' 
                self.sexfreq = 1 #primary pship normal frequency
        elif tag == 'comsex':
            self.tag = 'comsex' 
            #set the relative frequency for non 'primary' partners
            self.sexfreq = params.get('comsexSexfreq', 1)

    def candidate(self, day, beta):
        """Return date of HIV transmission"""
        male, female = self._mf
        if self.tag == 'comsex' or not male.miner:
            return day + self._prng.geometric(beta)
        else:
            return day + self.minerProlongation(day, self._prng.geometric(beta))

    def minerProlongation(self, day, days_to):
        """Extend interaction with primary and secondarypartners whie in mine
        """
        yearDay = day % 365 
        days_mining = self.params['days_mining']
        noMineDays = 365-days_mining
        extended = days_to//noMineDays*365 + (days_to % noMineDays)
        if noMineDays < yearDay: #if current day is in mining window extend
            extended += days_mining - yearDay

        return extended


    def __str__(self):
        return"""
        I am a {cls} class.
        Partners are: {m} and {f}
        """.format(**dict(
            cls=self.__class__,
            m = slef._mf[0],
            f = self._mf[1]
            ))

#Test this class
if __name__=="__main__":
    from c_Person import Person
    from numpy.random import RandomState
    seed=51
    pRNG = RandomState(seed=seed)


    pM = Person('M', None, dict(prng=pRNG))
    pF = Person('F', None, dict(prng=pRNG))

    part = Partnership(pM, pF, 458, None, dict(prng=pRNG, sigma01=1))

    part.transmission_date = 50
    print part.transmission_date 
    print part.tag 
