"""Provides parameter sets (as dicts) for simulations.

shared_params holds the common values across models.
These are picked to match EHG paper,
with three very slight differences in parameterization
(chosen for robustness to possible experiments).

"""

shared_params = dict(
    sScenarioName = '', #name of simulation model, and usu. folder for output
    nSim = 100,  #the number of replications for each parameter set (each value of epsilon)
    #equal number of males & females
    nMF = (10000,10000),
    pSeedHIV = (0.01,0.01),
    nBurnDays = 365*5,
    nSimDays=365*250,
    nOutputInterval = 365,
    nRndSeed = 7930881,  #fr EHG, but will be augmented based on sim specification
    #daily infection rate during each stage: a sequence
    beta_M2F = None,
    beta_F2M = None,
    #durations during each stage: a sequence
    durationsHIV = None,
    #aggregate partnership formation paramter
    rho = None,
    sigma01 = None
    )

def phi_ehg(male, female, epsilon):
    """Return float, the probability of partnership formation,
    **given** that ``male`` and ``female`` are selected as potential partners.
    Based on individuals' current number of partnerships, and mixing parameter
    :note: epsilon measures resistence to concurrency;
      it is 1 if no concurrency and 0 if no resistance to concurrency
    :note: we will use closure to set the `epsilon` local to the `sim`
    """
    if ( female.n_partners==0 and male.n_partners==0 ):
        return 1.0
    else:
        return (1.0-epsilon)  #epsilon controls concurrency


class mk():
    pass
mk.rho = 0.01 
mk.sigma = 0.005
mk.sigma02 = 1 / (30 * 28.4)

# beta params are from EHG's code: concprimaryinf/R/conc.sim.R
# (because reported there with more precision than in EHG Table 1)
beta_p = 0.007315068
beta_a =0.0002904110
beta_s = 0.002082192
beta_0 = 0
# dur params are from EGH's Table 1
dur_p = 88
dur_a = 3054
dur_s = 274
dur_0 = 307


#get the shared params
ehg_staged01 = shared_params.copy()
#params for the core EHG staged simulations
beta_ehg = (beta_p, beta_a, beta_s, beta_0)
#update the sim specific params
ehg_staged01.update(
    sim_name = 'ehg-staged',  #CHK do I use this? overridden in get_param_set
    rho = mk.rho,       #aggregate partnership formation paramter
    sigma01 = mk.sigma,
    sigma02 = mk.sigma02,
    #daily infection rate during each stage: a sequence
    beta_M2F = beta_ehg,
    beta_F2M = beta_ehg,
    #durations during each stage: a sequence
    durationsHIV = (dur_p, dur_a, dur_s, dur_0),
    )


